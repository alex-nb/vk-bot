require('dotenv').config();
const VkBot = require('node-vk-bot-api');
const Markup = require('node-vk-bot-api/lib/markup');
const Session = require('node-vk-bot-api/lib/session');
const Stage = require('node-vk-bot-api/lib/stage');
const Scene = require('node-vk-bot-api/lib/scene');

const bot = new VkBot(process.env.TOKEN);
const session = new Session();

const defaultKeyboard = Markup.keyboard([
    [
        Markup.button('Проверить наличие тестов', 'primary', { command: 'Начать' })
    ]
]);

const choose_test = new Scene('choose_test',
    (ctx) => {
        ctx.scene.next();
        ctx.reply('Есть следующие тесты для прохождения', null,  Markup.keyboard([
            [
                Markup.button(
                    'Тест 1 (легкий, 5 мин) Попытки: 10',
                    'positive',
                    {id:'123', title:'Тест 1'}
                ),
            ],
            [
                Markup.button(
                    'Тест 2 (легкий, 3 мин). Попытки: 5',
                    'positive',
                    {id:'456', title:'Тест 2'}
                ),
            ]
        ]).oneTime());
    },
    (ctx) => {
        if (ctx.message && ctx.message.payload) {
            const action = JSON.parse(ctx.message.payload);
            ctx.session.idTest = action.id;
            ctx.reply(`Вы выбрали ${action.title}`, null,  Markup.keyboard([
                [
                    Markup.button(
                        'Пройти',
                        'primary',
                        { command: 'start'}
                    )
                ],
            ]).oneTime());
        }
        ctx.scene.leave();
    });

const start_testing = new Scene('start_testing',
    (ctx) => {
        ctx.session[ctx.session.idTest] = [];
        ctx.scene.next();
        ctx.reply('Сколько будет 2+2?', null,  Markup.keyboard([
            [
                Markup.button('4', 'primary', {id:'1', correct: true, idQuest: '1'}),
                Markup.button('5', 'primary', {id:'2', correct: false, idQuest: '1'}),
                Markup.button('12', 'primary', {id:'3', correct: false, idQuest: '1'}),
                Markup.button('Не знаю', 'primary', {id:'4', correct: false, idQuest: '1'}),
            ]
        ]).oneTime());
    },
    (ctx) => {
        if (ctx.message && ctx.message.payload) {
            const action = JSON.parse(ctx.message.payload);
            ctx.session[ctx.session.idTest].push({id: action.id, correct: action.correct, idQuest: action.idQuest});
        }
        ctx.reply('Сколько будет 6/2?', null,  Markup.keyboard([
            [
                Markup.button('8', 'primary', {id:'1', correct: false, idQuest: '2'}),
                Markup.button('3', 'primary', {id:'2', correct: true, idQuest: '2'}),
                Markup.button('Буква И', 'primary', {id:'3', correct: false, idQuest: '2'}),
                Markup.button('42', 'primary', {id:'4', correct: false, idQuest: '2'}),
            ]
        ]).oneTime());
        ctx.scene.next();
    },
    (ctx) => {
        if (ctx.message && ctx.message.payload) {
            const action = JSON.parse(ctx.message.payload);
            ctx.session[ctx.session.idTest].push({id: action.id, correct: action.correct, idQuest: action.idQuest});
        }
        ctx.reply('Что из перечисленного умеет летать (с помощью крыльев)?', null,  Markup.keyboard([
            [
                Markup.button('Кошка', 'primary', {id:'1', correct: false, idQuest: '3'}),
                Markup.button('Слон', 'primary', {id:'2', correct: false, idQuest: '3'}),
                Markup.button('Рыба', 'primary', {id:'3', correct: false, idQuest: '3'}),
                Markup.button('Голубь', 'primary', {id:'4', correct: true, idQuest: '3'}),
            ]
        ]).oneTime());
        ctx.scene.next();
    },
    (ctx) => {
        if (ctx.message && ctx.message.payload) {
            const action = JSON.parse(ctx.message.payload);
            ctx.session[ctx.session.idTest].push({id: action.id, correct: action.correct, idQuest: action.idQuest});
        }
        ctx.reply('Сколько зубов в норме у взрослого человека?', null,  Markup.keyboard([
            [
                Markup.button('32', 'primary', {id:'1', correct: true, idQuest: '4'}),
                Markup.button('2', 'primary', {id:'2', correct: false, idQuest: '4'}),
                Markup.button('0', 'primary', {id:'3', correct: false, idQuest: '4'}),
                Markup.button('14', 'primary', {id:'4', correct: false, idQuest: '4'}),
            ]
        ]).oneTime());
        ctx.scene.next();
    },
    (ctx) => {
        if (ctx.message && ctx.message.payload) {
            const action = JSON.parse(ctx.message.payload);
            ctx.session[ctx.session.idTest].push({id: action.id, correct: action.correct, idQuest: action.idQuest});
        }
        ctx.reply('Сколько весит 1 кг. гвоздей?', null,  Markup.keyboard([
            [
                Markup.button('5 тонн', 'primary', {id:'1', correct: false, idQuest: '5'}),
                Markup.button('80 кг', 'primary', {id:'2', correct: false, idQuest: '5'}),
                Markup.button('1 кг', 'primary', {id:'3', correct: true, idQuest: '5'}),
                Markup.button('100 гр', 'primary', {id:'4', correct: false, idQuest: '5'}),
            ]
        ]).oneTime());
        ctx.scene.next();
    },
    (ctx) => {
        if (ctx.message && ctx.message.payload) {
            const action = JSON.parse(ctx.message.payload);
            ctx.session[ctx.session.idTest].push({id: action.id, correct: action.correct, idQuest: action.idQuest});
        }
        let allAnswer = 0;
        let correctAnswer = 0;
        ctx.session[ctx.session.idTest].map( question => {
            allAnswer++;
            question.correct ? correctAnswer++ : null;
        });
        ctx.reply(`Тест завершен. Правильных ответов: ${correctAnswer}/${allAnswer}. Спасибо.`, null, defaultKeyboard);
        delete ctx.session[ctx.session.idTest];
        ctx.scene.leave();
    });

const stage = new Stage(choose_test, start_testing);

bot.use(session.middleware());
bot.use(stage.middleware());

bot.button({ command: 'Начать' }, (ctx) => {
    ctx.scene.enter('choose_test');
});

bot.command('Начать', (ctx) => {
    ctx.scene.enter('choose_test');
});

bot.button({ command: 'start' }, (ctx) => {
    ctx.scene.enter('start_testing');
});

bot.startPolling();